import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import moduleName from '../node_modules/bootstrap/dist/css/bootstrap.css'

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
